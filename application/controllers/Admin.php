<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Admin extends REST_Controller{

  public function __construct($config = 'rest')
  {
    parent::__construct($config);
    $this->load->database();
  }

  function index_get()
  {
    $id = $this->get('id_admin');
    if ($id == '') {
      $gejala = $this->db->get('admin')->result();
    }else {
      $this->db->where('id_admin', $id);
      $gejala = $this->db->get('admin')->result();
    }
    $this->response(array('result' => $gejala), 200);
  }

  function index_post()
  {
    $username = $this->post('user');
    $password = $this->post('pass');

    $query = $this->db->get_where('admin', array(
										'username' => $username,
										'password' => $password
										));

    if ($query->num_rows() > 0) {
      $this->response(array('result' => $query), 200);
    }else {
      $this->response(array('result' => 'fail'), 502);
    }
  }

}
