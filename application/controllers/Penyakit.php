<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Penyakit extends REST_Controller{

  public function __construct($config = 'rest')
  {
    parent::__construct($config);
    $this->load->database();
  }

  function index_get()
  {
    $id = $this->get('id_penyakit');
    if ($id == '') {
      $gejala = $this->db->get('penyakit')->result();
    }else {
      $this->db->where('id_penyakit', $id);
      $gejala = $this->db->get('penyakit')->result();
    }
    $this->response(array('result' => $gejala), 200);
  }

  function index_post()
  {
    $data = array('id_penyakit' => $this->post('id'),
                  'nama' => $this->post('penyakit'));
    $insert = $this->db->insert('penyakit', $data);
    if ($insert) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_put()
  {
    $id = $this->put('id');
    $data = array('id_penyakit' => $this->put('id'),
                  'nama' => $this->put('penyakit'));
    $this->db->where('id_penyakit', $id);
    $update = $this->db->update('penyakit', $data);
    if ($update) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_delete()
  {
    $id = $this->delete('id');
    $this->db->where('id_penyakit', $id);
    $delete = $this->db->delete('penyakit');
    if ($delete) {
      $this->response(array('status' => 'sucess', 201));
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

}
