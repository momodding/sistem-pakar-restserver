<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Rule extends REST_Controller{

  public function __construct($config = 'rest')
  {
    parent::__construct($config);
    $this->load->database();
  }

  function index_get()
  {
    $id = $this->get('id_cf');
    if ($id == '') {
      $gejala = $this->db->get('cf_table')->result();
    }else {
      $this->db->where('id_cf', $id);
      $gejala = $this->db->get('cf_table')->result();
    }
    $this->response(array('result' => $gejala), 200);
  }

  function index_post()
  {
    $data = array('id_cf' => $this->post('cf'),
                  'id_gejala' => $this->post('gejala'),
                  'id_penyakit' => $this->post('penyakit'),
                  'mb' => $this->post('mb'),
                  'md' => $this->post('md'));
    $insert = $this->db->insert('cf_table', $data);
    if ($insert) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_put()
  {
    $id = $this->put('id');
    $data = array('id_cf' => $this->put('cf'),
                  'id_gejala' => $this->put('gejala'),
                  'id_penyakit' => $this->put('penyakit'),
                  'mb' => $this->put('mb'),
                  'md' => $this->put('md'));
    $this->db->where('id_cf', $id);
    $update = $this->db->update('cf_table', $data);
    if ($update) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_delete()
  {
    $id = $this->delete('id');
    $this->db->where('id_cf', $id);
    $delete = $this->db->delete('cf_table');
    if ($delete) {
      $this->response(array('status' => 'sucess', 201));
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

}
