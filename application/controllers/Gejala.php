<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Gejala extends REST_Controller{

  public function __construct($config = 'rest')
  {
    parent::__construct($config);
    $this->load->database();
  }

  function index_get()
  {
    $id = $this->get('id_gejala');
    if ($id == '') {
      $gejala = $this->db->get('gejala')->result();
    }else {
      $this->db->where('id_gejala', $id);
      $gejala = $this->db->get('gejala')->result();
    }
    //$this->response($gejala, 200);
    $this->response(array('result' => $gejala),200);
  }

  function index_post()
  {
    $data = array('id_gejala' => $this->post('id'),
                  'gejala' => $this->post('gejala'));
    $insert = $this->db->insert('gejala', $data);
    if ($insert) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_put()
  {
    $id = $this->put('id');
    $data = array('id_gejala' => $this->put('id'),
                  'gejala' => $this->put('gejala'));
    $this->db->where('id_gejala', $id);
    $update = $this->db->update('gejala', $data);
    if ($update) {
      $this->response($data, 200);
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

  function index_delete()
  {
    $id = $this->delete('id');
    $this->db->where('id_gejala', $id);
    $delete = $this->db->delete('gejala');
    if ($delete) {
      $this->response(array('status' => 'sucess', 201));
    }else {
      $this->response(array('status' => 'fail', 502));
    }
  }

}
